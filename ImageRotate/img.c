#include <inttypes.h>
#include <stdlib.h>

#include "include/img.h"
#include "include/err.h"

void destroy_image_file(struct image_file file){
	free(*file.extra_bytes);
	free(file.img->data);
}

void read_image(
	enum read_status (from_func) (FILE*, const struct image_file),
	const char* file_name,
	struct image_file image
	){
	FILE* img_orig = fopen(file_name, "rb");
	if (!img_orig) {
		print_read_status(READ_INVALID_SIGNATURE);
		exit(EXIT_FAILURE);
	}
	enum read_status err = from_func(img_orig, image);
	if (err != READ_OK) {
		fclose(img_orig);
		print_read_status(err);
		exit(EXIT_FAILURE);
	}

	fclose(img_orig);
}

void write_image(
	enum write_status (to_func) (FILE*, const struct image_file),
 	const char* file_name,	
	struct image_file image
	){
	FILE* img_rotated = fopen(file_name, "wb");
	enum write_status err = to_func(img_rotated, image);
	if (err != WRITE_OK) {
		fclose(img_rotated);
		print_write_status(err);
		exit(EXIT_FAILURE);
	}
	fclose(img_rotated);
}

enum read_status from_bmp(FILE* fin, const struct image_file image) {
	if (!fin || !image.img) return READ_INVALID_SIGNATURE;

	if (!read_header(fin, image.header)) return READ_INVALID_HEADER;
	if (image.header->biBitCount != 24) return READ_INVALID_BMP_FORMAT;

	*image.extra_bytes = malloc(image.header->bfOffbits - sizeof(struct bitmap_header));
	fread(*image.extra_bytes, image.header->bfOffbits - sizeof(struct bitmap_header), 1, fin);

	image.img->height = image.header->biHeight;
	image.img->width = image.header->biWidth;

	image.img->data = malloc(sizeof(struct pixel) * image.img->width  * image.img->height);

	fseek(fin, image.header->bfOffbits, SEEK_SET);
	for (size_t y = 0; y < image.img->height; y += 1) {
		fread(
			&image.img->data[y * image.img->width],
			sizeof(struct pixel),
			image.img->width,
			fin
		);

		fseek(fin, calculate_padding(image.img->width, image.header->biBitCount), SEEK_CUR);
	}
	return READ_OK;
}

enum write_status to_bmp(FILE* fout, const struct image_file image) {
	image.header->biWidth = image.img->width;
	image.header->biHeight = image.img->height;
	fwrite(image.header, sizeof(struct bitmap_header), 1, fout);
	fwrite(*image.extra_bytes, image.header->bfOffbits - sizeof(struct bitmap_header), 1, fout);
	for(size_t j = 0; j < image.img->height; j+=1) {
		fwrite(
			&image.img->data[j * image.img->width],
			sizeof(struct pixel),
			image.img->width,
			fout
		);

		uint8_t _p = 0x0;
		fwrite(&_p, 1, calculate_padding(image.img->width, image.header->biBitCount), fout);
	}

	return  WRITE_OK;
}

size_t calculate_padding(const size_t w, const size_t c) { 
	return ((4 - (w * (c / 8)) % 4) & 3); 
}

void rotate_pixels(struct pixel from[], struct pixel to[], uint32_t w, uint32_t h) {
	int64_t x = 0;
	for (size_t i = 0; i < w; i += 1) {
		for(int64_t j = h-1; j >= 0; j -= 1) { //int64_t does magic. so it should.
			*( to + i*h + (x++)) = *(from + j*w + i);
		}
		x = 0;
	}
}

struct image rotate_image( const struct image* orig) {
	struct image to;

	to.width  = orig->height;
	to.height = orig->width;
	to.data = malloc(sizeof(struct pixel) * orig->width * orig->height);

	rotate_pixels(
		orig->data,
		to.data,
		orig->width,
		orig->height
	);

	return to;
}
