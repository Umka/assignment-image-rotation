#ifndef _ERR_H
#define _ERR_H

//enumeration of status codes when reading
enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_INVALID_BMP_FORMAT
};

//enumeration of status codes when writting
enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR
};

//an array of error messages when reading
//corresponding to the number in the enumeration
extern const char* read_status_descr[];

//an array of error messages when writting
//corresponding to the number in the enumeration
extern const char* write_status_descr[];

//a function that prints an error while reading into an error stream
void print_read_status(enum read_status);
//a function that prints an error while writting into an error stream
void print_write_status(enum write_status);

#endif // _ERR_H
