#ifndef _IMG_H
#define _IMG_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <malloc.h>

#include "bmp.h"
#include "err.h"

//image structure without unnecessary overhead.
//Сontains only height, width and pixel grid
struct image {
  uint64_t width, height;
  struct pixel* data;
};

//structure of one pixel.
//Сontains 3 bytes of colors: red, blue and green
struct pixel {
	uint8_t r, g, b;
};

//the complete structure of the image file.
//Contains the header block of the image file,
//additional bytes between the header and the pixel grid,
//and the pixel grid.
struct image_file
{
  struct bitmap_header* header;
  uint8_t** extra_bytes;
  struct image* img;
};

//a function that destroy image file
void destroy_image_file(struct image_file);

//a function that reads an image file into 'struct image_file'
void read_image(enum read_status (from_func) (FILE*, struct image_file), const char*, const struct image_file);

//a function that writes the image ('struct image_file') to a named file
void write_image(enum write_status (to_func) (FILE*, struct image_file), const char*, const struct image_file);


//a function that reads BMP image from an open file into 'struct image_file'
enum read_status from_bmp (FILE*, const struct image_file);

//a function that writes the BMP image ('struct image_file') to a open named file
enum write_status to_bmp(FILE*, const struct image_file);

//function that calculates the paddings between the lines of the image
size_t calculate_padding(const size_t, const size_t);

//a function that rotates the grid of pixels counterclockwise by 90 degrees
//and returns the resulting grid
struct image rotate_image( const struct image*);

#endif // _IMG_H
