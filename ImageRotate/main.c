#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <malloc.h>

#include "include/img.h"
#include "include/err.h"

int main(int argc, char** argv) {
	if (argc == 1) {
		fprintf(stderr, "Specify file name.\n");
		exit(EXIT_FAILURE);
	}

	struct bitmap_header header = { 0 };
	uint8_t* extra_bytes = NULL;
	struct image our_image = { 0 };

	struct image_file full_image = {
		&header,
		&extra_bytes,
		&our_image
	};

	//read the image file specified as an argument when starting the program
	read_image (from_bmp, argv[1], full_image);

	//rotate the image pixel grid
	our_image = rotate_image(&our_image);
	//replace the image with an inverted one
	full_image.img = &our_image;

	//write the original image file but with an inverted grid of pixels
	write_image (to_bmp, "rot.bmp", full_image);

	//free the allocated memory
	destroy_image_file(full_image);

	puts("\n");
	return 0;
}
