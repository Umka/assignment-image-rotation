#include <stdio.h>

#include "include/err.h"

const char* read_status_descr[] = {
	[READ_OK] = "Read: ok.",
	[READ_INVALID_SIGNATURE] = "Read: invalid signature.",
	[READ_INVALID_BITS] = "Read: invalid bits.",
	[READ_INVALID_HEADER] = "Read: invalid header.",
	[READ_INVALID_BMP_FORMAT] = "Read: invalid BMP format."
};

const char* write_status_descr[] = {
	[READ_OK] = "Write: ok.",
	[WRITE_ERROR] = "Write: smth went wrong."
};

void print_read_status(enum read_status status_code){
	fprintf(stderr, "%s\n", read_status_descr[status_code]);
}

void print_write_status(enum write_status status_code){
	fprintf(stderr, "%s\n", write_status_descr[status_code]);
}
